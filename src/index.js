const cliArgs = require('command-line-args');
const { buildGraph } = require('./graph');
const { msToString } = require('./utils');
const { connection, setupDb } = require('@krz-sad-tcc/db-tools');

const args = cliArgs([
  { name: 'init', type: Boolean },
  { name: 'graph', type: Number },
]);

const run = async (args) => {
  if (
    (!('init' in args) && !('graph' in args)) ||
    ('graph' in args &&
      (isNaN(args.graph) || args.graph === null || args.graph < 0))
  ) {
    console.log('Usage: node src/index.js [--init] [--graph <num_games>]');
    process.exit(1);
  }

  if ('init' in args) {
    await setupDb(
      connection,
      process.env.DB_NAME,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  }

  if ('graph' in args) {
    const start = Date.now();
    const sleep = 1000;
    const db = connection.database(process.env.DB_NAME);
    await buildGraph(db, parseInt(args.graph, 10), sleep);
    const duration = Date.now() - start;
    console.log('Time elapsed:', msToString(duration));
  }
};

run(args);
