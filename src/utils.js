const axios = require('axios').default;
const { Chess } = require('chess.js');
const { LICHESS_EXPLORER_BASE_URL } = require('./const');

const chess = new Chess();

const buildEdge = (parent, child, move) => {
  return {
    _from: parent._id,
    _to: child._id,
    nGames: calculateNumberOfGames(move),
    moveSan: move.san,
  };
};

const buildVertex = (fen, onlineData, localData) => {
  return {
    epd: toEpd(fen),
    fen: fen,
    nGames: calculateNumberOfGames(onlineData),
    eco: localData?.eco || '',
    name: localData?.name || '',
    isLeaf: true,
  };
};

const calculateNumberOfGames = (data) => {
  let nGames = 0;

  nGames += 'white' in data ? data.white : 0;
  nGames += 'draws' in data ? data.draws : 0;
  nGames += 'black' in data ? data.black : 0;

  return nGames;
};

const fetchPositionData = async (fen, sleep) => {
  // Sleep to avoid rate limiting.
  await new Promise((resolve) => setTimeout(resolve, sleep));

  const { data } = await axios.get(
    `${LICHESS_EXPLORER_BASE_URL}${toUrlEncoding(fen)}`,
  );
  return data;
};

const msToString = (ms) => {
  let seconds = Math.floor(ms / 1000);
  let minutes = Math.floor(seconds / 60);
  let hours = Math.floor(minutes / 60);

  seconds = seconds % 60;
  minutes = minutes % 60;

  return `${hours} hours, ${minutes} minutes, ${seconds} seconds.`;
};

const toEpd = (fen) => {
  const fenParts = fen.split(' ');

  // Remove en passant field when it's not legal.
  chess.load(fen);
  const moves = chess.moves({ verbose: true });
  if (moves.every((m) => m.flags !== 'e')) {
    fenParts[3] = '-';
  }

  // Remove last 2 parts, which are move numbers.
  fenParts.splice(-2);

  return fenParts.join(' ');
};

const toUrlEncoding = (fen) => {
  return fen.replace(/ /g, '%20').replace(/\//g, '%2F');
};

module.exports = {
  buildEdge,
  buildVertex,
  calculateNumberOfGames,
  fetchPositionData,
  msToString,
  toEpd,
  toUrlEncoding,
};
