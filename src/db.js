const {
  COLL_LEADS_TO,
  COLL_POSITIONS,
  query,
  withErrorHandling,
} = require('@krz-sad-tcc/db-tools');
const { ROOT_FEN } = require('./const');

const addEdge = async (db, edge) => {
  return withErrorHandling(async () => {
    const leadsTo = db.collection(COLL_LEADS_TO);
    await leadsTo.save(edge);
    console.log('> Connected', edge._from, 'to', edge._to);
  });
};

const addVertex = async (db, vertex) => {
  return withErrorHandling(async () => {
    const positions = db.collection(COLL_POSITIONS);
    const result = await positions.save(vertex, {
      returnNew: true,
    });
    console.log('> Added', result.new._id, 'with', result.new.nGames, 'games');
    return result.new;
  });
};

const getAnyLeaf = async (db) => {
  return withErrorHandling(async () => {
    const result = await query(db, {
      query: `
        FOR v IN @@collection
          FILTER v.isLeaf == true
          LIMIT 1
          RETURN v
          `,
      bindVars: {
        '@collection': COLL_POSITIONS,
      },
    });
    return result;
  });
};

const getExpandableLeaves = async (db, minGames) => {
  return withErrorHandling(async () => {
    const result = await query(db, {
      query: `
        FOR v IN @@collection
          FILTER v.isLeaf == true AND v.nGames >= @minGames
          RETURN v
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
        minGames: minGames,
      },
    });
    return result;
  });
};

const getRoot = async (db) => {
  return withErrorHandling(async () => {
    const result = await query(db, {
      query: `
        FOR v IN @@collection
          FILTER v.fen == @initialFen
          RETURN v
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
        initialFen: ROOT_FEN,
      },
    });
    return result[0] || null;
  });
};

const getVertexByEpd = async (db, epd) => {
  return withErrorHandling(async () => {
    const result = await query(db, {
      query: `
        FOR v IN @@collection
          FILTER v.epd == @epd
          LIMIT 1
          RETURN v
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
        epd: epd,
      },
    });
    return result[0] || null;
  });
};

const makeNotLeaf = async (db, id) => {
  return withErrorHandling(async () => {
    const positions = db.collection(COLL_POSITIONS);
    const result = await query(db, {
      query: `
        FOR v IN @@collection
          FILTER v._id == @id
          LIMIT 1
          RETURN v
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
        id: id,
      },
    });
    if (result[0].isLeaf) {
      const updatedVertex = await positions.update(
        { _id: id },
        { isLeaf: false },
        { returnNew: true },
      );
      return updatedVertex;
    }
    return result[0];
  });
};

module.exports = {
  addEdge,
  addVertex,
  getAnyLeaf,
  getExpandableLeaves,
  getRoot,
  getVertexByEpd,
  makeNotLeaf,
};
