const { Chess } = require('chess.js');
const fs = require('fs');
const { CHESS_OPENINGS_PATH, ROOT_FEN, ROOT_NAME } = require('./const');
const {
  addVertex,
  addEdge,
  getExpandableLeaves,
  getRoot,
  getVertexByEpd,
  makeNotLeaf,
} = require('./db');
const { buildEdge, buildVertex, fetchPositionData, toEpd } = require('./utils');

const chess = new Chess();

const jsonDb = JSON.parse(fs.readFileSync(CHESS_OPENINGS_PATH));

const findInJsonDb = (fen) => {
  const epd = toEpd(fen);
  return jsonDb.find((opening) => opening.epd == epd);
};

const addRoot = async (db, sleep) => {
  const rootOnlineData = await fetchPositionData(ROOT_FEN, sleep);
  const root = buildVertex(ROOT_FEN, rootOnlineData, {
    name: ROOT_NAME,
  });
  const dbRoot = await addVertex(db, root);
  return dbRoot;
};

const buildGraph = async (db, minGames, sleep) => {
  const root = await getRoot(db);

  if (!root) {
    await addRoot(db, sleep);
  }

  await expandGraph(db, minGames, sleep);
};

const expandGraph = async (db, minGames, sleep) => {
  let parent, parentOnlineData;
  let child, childOnlineData, childLocalData;
  let edge, move;
  let matchingVertex;

  const queue = [];
  const leaves = await getExpandableLeaves(db, minGames);
  queue.push(...leaves);

  while (queue.length !== 0) {
    parent = queue.shift();

    parentOnlineData = await fetchPositionData(parent.fen, sleep);
    const { moves } = parentOnlineData;

    for (let i = 0; i < moves.length; i++) {
      move = moves[i];
      chess.load(parent.fen);
      chess.move(move.san);
      matchingVertex = await getVertexByEpd(db, toEpd(chess.fen()));

      if (matchingVertex) {
        console.log('Position', toEpd(chess.fen()), 'already in DB...');
        edge = buildEdge(parent, matchingVertex, move);
        await addEdge(db, edge);
        continue;
      }

      childOnlineData = await fetchPositionData(chess.fen(), sleep);
      childLocalData = findInJsonDb(chess.fen());
      child = buildVertex(chess.fen(), childOnlineData, childLocalData);
      child = await addVertex(db, child);
      edge = buildEdge(parent, child, move);
      await addEdge(db, edge);

      if (child.nGames >= minGames) {
        queue.push(child);
      }
    }

    if (moves.length > 0) {
      await makeNotLeaf(db, parent._id);
    }
  }
};

module.exports = {
  addRoot,
  buildGraph,
  expandGraph,
};
