const dbVertices = {
  'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -': {
    _id: 'positions/2319948',
    _rev: '_ejp6Vd6-AH',
    _key: '2319948',
    epd: 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -',
    fen: 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2',
    nGames: 506921,
    eco: 'B20',
    name: 'Sicilian Defense',
    isLeaf: false,
  },
  'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -': {
    _id: 'positions/2320361',
    _rev: '_ejp6VeG-BA',
    _key: '2320361',
    epd: 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -',
    fen: 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2',
    nGames: 419447,
    eco: 'B27',
    name: 'Sicilian Defense',
    isLeaf: false,
  },
};

module.exports = { dbVertices };
