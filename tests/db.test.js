const {
  connection,
  dropDb,
  query,
  setupDb,
  COLL_LEADS_TO,
  COLL_POSITIONS,
} = require('@krz-sad-tcc/db-tools');
const { localData } = require('./fixtures/localData');
const { onlineData } = require('./fixtures/onlineData');
const { vertices } = require('./fixtures/vertices');
const { ROOT_FEN } = require('../src/const');
const {
  addEdge,
  addVertex,
  getAnyLeaf,
  getExpandableLeaves,
  getRoot,
  getVertexByEpd,
  makeNotLeaf,
} = require('../src/db');
const { buildVertex } = require('../src/utils');

const factory = (element, overwrite = {}) => {
  return {
    ...element,
    ...overwrite,
  };
};

const getAllEdges = async (db) => {
  let dbEdges = await query(db, {
    query: `
      FOR e IN @@collection
        RETURN e
    `,
    bindVars: {
      '@collection': COLL_LEADS_TO,
    },
  });
  return dbEdges;
};

global.console.log = jest.fn();

describe('#addVertex', () => {
  const testDbName = 'addVertex';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  const v0 = buildVertex(ROOT_FEN, onlineData['initial'], undefined);
  const v1 = buildVertex(
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2',
    onlineData['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'],
    localData['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'],
  );

  test('given an empty graph, it adds a vertex', async () => {
    await addVertex(db, v0);

    const result = await query(db, {
      query: `
        FOR v IN @@collection
          RETURN v.epd
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
      },
    });

    expect(result).toContain(
      'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -',
    );
  });

  test('given a graph with one vertex, it adds another vertex', async () => {
    await addVertex(db, v1);
    const result = await query(db, {
      query: `
        FOR v IN @@collection
          RETURN v.epd
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
      },
    });

    expect(result).toContain(
      'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -',
    );
  });
});

describe('#addEdge', () => {
  const testDbName = 'addEdge';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  test('given a graph with two vertices, it adds an edge between them', async () => {
    const epd0 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';
    const epd1 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -';
    const dbVertex0 = await addVertex(db, vertices[epd0]);
    const dbVertex1 = await addVertex(db, vertices[epd1]);
    const e0 = {
      _from: dbVertex0._id,
      _to: dbVertex1._id,
      nGames: 0,
      moveSan: 'x',
    };

    await addEdge(db, e0);

    const dbEdges = await getAllEdges(db);

    expect(dbEdges).toHaveLength(1);
    expect(dbEdges).toContainEqual(
      expect.objectContaining({
        _from: dbVertex0._id,
        _to: dbVertex1._id,
      }),
    );
  });
});

describe('#getVertexByEpd', () => {
  const testDbName = 'getVertexByEpd';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  test('given a graph with a vertex, it gets that vertex by epd', async () => {
    const epd = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';

    await addVertex(db, vertices[epd]);

    const v = await getVertexByEpd(db, epd);
    expect(v.epd).toEqual(epd);
  });
});

describe('#getAnyLeaf', () => {
  const testDbName = 'getAnyLeaf';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  test('given a graph with leaves, it can get any leaf', async () => {
    const epd = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';

    await addVertex(db, vertices[epd]);

    const leaves = await getAnyLeaf(db);
    expect(leaves).toHaveLength(1);
  });

  test("given a graph without leaves, it doesn't get any", async () => {
    const epd = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';
    const v0 = factory(vertices[epd], { isLeaf: false });

    await addVertex(db, v0);

    const leaves = await getAnyLeaf(db);
    expect(leaves).toHaveLength(0);
  });
});

describe('#getExpandableLeaves', () => {
  const testDbName = 'getExpandableLeaves';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  test('given a graph with leaves, it gets all of them when minGames is 0', async () => {
    const epd0 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';
    const epd1 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -';

    await addVertex(db, vertices[epd0]);
    await addVertex(db, vertices[epd1]);

    const leaves = await getExpandableLeaves(db, 0);

    expect(leaves.map((l) => l.epd)).toContain(epd0);
    expect(leaves.map((l) => l.epd)).toContain(epd1);
  });

  test("given a graph with leaves, it doesn't get any leaves when minGames is arbitrarily large", async () => {
    const epd0 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';
    const epd1 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -';

    await addVertex(db, vertices[epd0]);
    await addVertex(db, vertices[epd1]);

    const leaves = await getExpandableLeaves(db, 1000000000);
    expect(leaves).toHaveLength(0);
  });

  test('given a graph with leaves, it only gets the ones with nGames ≥ minGames', async () => {
    const epd0 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';
    const epd1 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -';
    const minGames = 1000;
    const v0 = factory(vertices[epd0], { nGames: minGames - 1 });
    const v1 = factory(vertices[epd1], { nGames: minGames });

    await addVertex(db, v0);
    await addVertex(db, v1);

    const leaves = await getExpandableLeaves(db, minGames);

    expect(leaves).toHaveLength(1);
    expect(leaves.map((l) => l.epd)).toContain(epd1);
  });
});

describe('#getRoot', () => {
  const testDbName = 'getRoot';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  test('given a graph with a root, it returns it', async () => {
    await addVertex(db, vertices['initial']);

    const root = await getRoot(db);

    expect(root).not.toBeNull();
  });

  test('given a graph without a root, it returns null', async () => {
    const root = await getRoot(db);

    expect(root).toBeNull();
  });

  test("given a graph with leaves, it doesn't get any leaves when minGames is arbitrarily large", async () => {
    const epd0 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';
    const epd1 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -';

    await addVertex(db, vertices[epd0]);
    await addVertex(db, vertices[epd1]);

    const leaves = await getExpandableLeaves(db, 1000000000);
    expect(leaves).toHaveLength(0);
  });

  test('given a graph with leaves, it only gets the ones with nGames ≥ minGames', async () => {
    const epd0 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';
    const epd1 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -';
    const minGames = 1000;
    const v0 = factory(vertices[epd0], { nGames: minGames - 1 });
    const v1 = factory(vertices[epd1], { nGames: minGames });

    await addVertex(db, v0);
    await addVertex(db, v1);

    const leaves = await getExpandableLeaves(db, minGames);

    expect(leaves).toHaveLength(1);
    expect(leaves.map((l) => l.epd)).toContain(epd1);
  });
});

describe('#makeNotLeaf', () => {
  const testDbName = 'makeNotLeaf';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  test('given a leaf, it can turn it into not leaf', async () => {
    const dbVertex0 = await addVertex(db, vertices['initial']);

    let leaves = await getExpandableLeaves(db, 0);
    expect(leaves).toHaveLength(1);

    await makeNotLeaf(db, dbVertex0._id);

    leaves = await getExpandableLeaves(db, 0);
    expect(leaves).toHaveLength(0);
  });

  test("given a vertex which is not a leaf, it doesn't do anything", async () => {
    const v0 = factory(vertices['initial'], { isLeaf: false });

    const dbVertex0 = await addVertex(db, v0);

    let leaves = await getExpandableLeaves(db, 0);
    expect(leaves).toHaveLength(0);

    await makeNotLeaf(db, dbVertex0._id);

    const dbVertex1 = await getVertexByEpd(db, vertices['initial'].epd);
    expect(dbVertex0).toEqual(dbVertex1);
  });
});
