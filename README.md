# Chess Openings Graph Builder

This project aims to build a graph database of chess openings. It uses the [chess-openings][1] repository and the [lichess API][2] to get information. The generated graph is an [ArangoDB][3] graph.

Recently generated graphs are stored in the [db-data][4] repository.

## Requirements

- Node.js 14
- arangodb
- python3
- python-chess

We recommend the use of [NVM][5] to manage Node.js versions.

To install python-chess:

```bash
pip3 install chess
```

## Run

General usage:

```bash
yarn build-graph [--init] [--graph <num_games>]
```

Create a new graph (or overwrite the one you already have), without populating it:

```bash
yarn build-graph --init
```

Extend the graph you already have:

```bash
yarn build-graph --graph <num_games>
```

## Tools

Export the database into an output directory named "db-data" overwriting any data already there:

```bash
yarn export-db
```

Import data previously exported:

```bash
yarn import-db
```

Generate an updated json file from the chess-openings repository:

```bash
./bin/update.sh
```

[1]: https://github.com/lichess-org/chess-openings
[2]: https://lichess.org/api#tag/Opening-Explorer
[3]: https://www.arangodb.com/
[4]: https://gitlab.com/krz-sad-tcc/db-data
[5]: https://github.com/nvm-sh/nvm
