#!/bin/bash

git submodule foreach git pull --ff-only origin
python3 ../chess-openings/bin/to-json.py ../chess-openings/*.tsv > \
../chess-openings-db/chess-openings.json
