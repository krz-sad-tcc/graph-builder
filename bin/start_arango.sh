#!/bin/sh

arangod \
  --daemon \
  --pid-file /tmp/server.pid \
  --server.endpoint tcp://0.0.0.0:8529 \
  --database.password "$DB_PASS"
